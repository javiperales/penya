<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', 'Api\ProductController@index'); //controladores de api
Route::get('products/{id}', 'Api\ProductController@show'); //para ver la vista
Route::post('products', 'Api\ProductController@store'); //para ver la vista
Route::put('products/{id}', 'Api\ProductController@update'); //para ver la vista
Route::delete('products/{id}', 'Api\ProductController@destroy'); //para ver la vista

