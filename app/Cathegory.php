<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cathegory extends Model
{
    protected $fillable = [
        'name'
    ];

      public function product()
    {
      return  $this->hasMany(\App\Product::class);
    }
}
