<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Product;
use PDF;
;

class OrderController extends Controller
{
   public function __construct()
   {
    $this->middleware('auth');

}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function saveBasket(){



    }

    public function generatePDF($id)
    {
        $order=Order::findOrFail($id);
        $products = $order->pedidosProducto;
        $total = 0;

         foreach ($products as $product) {
            $total += $product->price*$product->pivot->quantity;
        }

        $data = [
            'order'=> $order,
            'products'=>$products,
            'total'=>$total,
        ];

        $pdf =PDF::loadView('pedidoPdf', $data);

        return $pdf->stream('pedido')->header('Content-Type', 'application/pdf');

    }
    public function index()
    {

        $order = Order::Paginate(10);
        return view('order.index',['order'=>$order]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order=Order::findOrFail($id);
        $products = $order->pedidosProducto;
        $total = 0;

        foreach ($products as $product) {
            $total += $product->price*$product->pivot->quantity;
        }
        //return $products;
        $this->authorize('view', $order);
        return view('order.show',['order'=>$order],['products'=>$products ,'total'=>$total]);
    }


    public function edit($id)
    {
        $order=Order::findOrFail($id);
        $this->authorize('view', $order);
        return view('order.edit', ['order'=>$order]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $order->fill($request->all()); //coge todo y guarda en order
       // $this->authorize('update',$order);
        $order->save();
        return redirect('/order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $pedidosProducto=Order::findOrFail($id);
       Order::find($id)->products()->detach();
       $pedidosProducto->delete();
       return back();

    }
}
