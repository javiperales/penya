<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cathegory =Cathegory::all();
        $products = Product::paginate(5);
        return view('product.index',['products'=>$products], ['cathegory'=>$cathegory]);
    }


    public function create()
    {
        $categorias =Cathegory::all();
        return view('product.create', ['categorias'=>$categorias]);


    }


  public function store(Request $request)
    {

        $reglas = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'cathegory_id' => 'min:1'
        ];
        $request->validate($reglas);
        $product = new Product();
        $product->fill($request->all());
        $product->save();
        return redirect('/product');
    }

    public function show($id)
    {
         $product = Product::findOrFail($id);
          $cathegory = Cathegory::all();
        return view('product.show', [
            'cathegory' => $cathegory
        ],['product'=>$product]);

    }

    public function edit($id)
    {
         $product = Product::findOrFail($id);
        return view('product.edit', [
            'product' => $product
        ]);
    }

    public function update(Request $request, $id)
    {

        $reglas = [
            'name'=> 'required|max:255|min:3',
            'price'=> 'required|numeric',
            'cathegory_id'=> 'min:1'
        ];
        $request->validate($reglas);
        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();
        return redirect('/product');
    }

    public function destroy($id)
    {

        Product::destroy($id);

        return back();
    }
}
