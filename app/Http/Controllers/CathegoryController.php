<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\Product;

class CathegoryController extends Controller
{
  public function __construct()
  {
    $cathegories = Cathegory::paginate(5);

    $this->middleware('auth')->except('index');


}

public function index()
{
    $cathegories = Cathegory::paginate(5);
    return view('cathegory.index',['cathegories'=>$cathegories]);
}


public function create()
{
    return view('cathegory.create');
}


public function store(Request $request)
{
    $reglas = [
        'name' => 'required|max:255|min:3'
    ];
    $request->validate($reglas);
    $category = new Cathegory();
    $category->fill($request->all());
    $category->save();
    return redirect('/cathegory');
}


public function show($id)
{
  $product = Product::all();

  $cathegory = Cathegory::findOrFail($id);
  return view('cathegory.show', [
    'cathegory' => $cathegory
],['product'=>$product]);

}


public function edit($id)
{

    $cathegory = Cathegory::findOrFail($id);
    return view('cathegory.edit',['cathegory'=>$cathegory]);
}


public function update(Request $request, $id)
{
   $reglas = [
    'name'=> 'required|max:255|min:3'
];
$request->validate($reglas);
$categoria = Cathegory::findOrFail($id);
$categoria->fill($request->all());
$categoria->save();
return redirect('/cathegory');
}


public function destroy($id)
{
    Cathegory::destroy($id);
    return back();
}
}
