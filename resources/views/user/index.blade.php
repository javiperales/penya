@extends('layouts.app')

@section('content')
<h1>Lista de usuarios</h1>
<a href="/users/create">Nuevo</a>

<table class="table">
    <tr>
        <td>Nombre</td>
        <td>Email</td>
        <td>Acciones</td>
    </tr>


    @forelse ($users as $user)
    <tr>
        <td> {{ $user->name }} </td>
        <td> {{ $user->email }} </td>
        <td> {{ $user->role->name }} </td>
        <td>
            <a href="/users/{{ $user->id }}/edit">Editar</a>
                @can('delete', $user)
            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
                @endcan

                <a class="btn btn-success" href="/group/{{$user->id}}">guardar</a>
            </form>

            <a href="/users/{{$user->id}}">Ver</a>
        </td>

</tr>

@empty
<li>No hay usuarios!!</li>
@endforelse
</table>
{{ $users->render() }}

@endsection
