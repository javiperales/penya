@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear nuevo producto<br>
                    <a href="/products">Lista de productos</a></div>

                    <div class="card-body">
                        <form method="post" action="/product">
                            {{ csrf_field() }}
                            <label>Nombre:</label>
                            <input type="text" name="name" value="{{ old('name') }}">
                            <div class="alert alert-danger">
                                {{ $errors->first('name')}}
                            </div>
                            <label>Precio</label>
                            <input type="text" name="price" value=" {{ old('price') }}">
                            <div class="alert alert-danger">
                                {{ $errors->first('price') }}
                            </div>
                            <br>


                            <label> Categoría: </label>
                            <select name="cathegory_id">
                                @foreach ($categorias as $categoria)
                                <option value="{{ $categoria->id }}"
                                    {{ old('cathegory_id') == $categoria->id ?
                                    'selected="selected"' :
                                    ''
                                }}>{{ $categoria->name }}
                            </option>
                            @endforeach
                            <div class="alert alert-danger">
                                {{ $errors->first('cathegory_id') }}
                            </div>
                        </select>
                        <br>
                        @if(!count($categorias)==0)
                        <input type="submit" value="Crear">
                        @else
                        <div class="alert alert-danger">Crea una categoria
                            <br>
                            <a href="/cathegory/create">Crear categoria</a>
                        </div>
                        @endif

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
