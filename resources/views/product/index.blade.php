@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">lista de productos<br>

          <a href="/product/create">crear nuevo producto</a>

          <div class="card-body">
           <table class="table">
            <tr>
              <td>id</td>

              <td>nombre</td>
              <td>categoria</td>
              <td>precio</td>
              <td>acciones</td>
              <td>carrito</td>
            </tr>
            @forelse($products as $producto)
            <tr>
              <td>{{$producto->id}}</td>
              <td>{{$producto->name}}</td>
              @foreach ($cathegory as $categoria)
              @if($producto->cathegory_id == $categoria->id)
              <td>{{ $categoria->name}}</td>
              @endif
              @endforeach


              <td>{{$producto->price}}</td>

              <td>
                <a href="/product/{{$producto->id}}/edit" class="btn btn-info" role="button">editar</a>
                <a href="/product/{{$producto->id}}" class="btn btn-info" role="button">ver</a>
                <form method="post" action="/product/{{ $producto->id }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                  <input type="submit" value="borrar">
                </form>
                <td>
                  <a href="/basket/{{$producto->id}}" class="btn btn-info" role="button">añadir carrito</a>
                </td>

              </td>
            </tr>

            @empty
            <h1>no hay productos que mostrar</h1>

            @endforelse

          </table>
          {{ $products->render() }}

          <br>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection
