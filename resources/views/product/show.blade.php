@extends('layouts.app')

@section('title', 'Product')

@section('content')

<h1>
    Este es el detalle productos <?php echo $product->id ?>
</h1>

<ul>
    <li>nombre: {{ $product->name }}</li>
    <li>precio: {{ $product->price }}</li>
    <li>
    @foreach ($cathegory as $categoria)
    @if($product->cathegory_id == $categoria->id)
    {{ $categoria->name}}
    @endif
    @endforeach
</li>
</ul>

@endsection
